class StarLists {

    /**
     * 
     * @param {string} level 
     */
    constructor(level) {
        this.level = level
        this.starList = [];
        this.elementDraw = null;
    }

    /**
     * 
     * @param {*} starNumber 
     * @param {*} starX 
     * @param {*} starY 
     */
    createStar(starNumber, starX, starY) {
        this.starList.push(new Stars(this.level, starNumber, starX, starY));
        this.draw();
    }


    draw() {

        if (!this.elementDraw) {
            this.elementDraw = document.createElement('div');
        }

        if (this.elementDraw) {
            this.elementDraw.innerHTML = '';
        }

        for (let i = 0; i < this.starList.length; i++) {
            this.starList[i].draw();
            this.elementDraw.appendChild(this.starList[i].starDraw);
        }

    }



    isWin() {
        let rightPosition = 0
        for (let i = 0; i < this.starList.length; i++) {
            if (this.starList[i].currentRotation === 8) {
                rightPosition++;
            }
        }

        console.log("il y a " + rightPosition + "etoiles en place")

        if (rightPosition === this.starList.length) {
            return true;

        } else {
            return false;}
    }
}