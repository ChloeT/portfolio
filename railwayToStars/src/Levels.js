class Levels {
    /**
     * 
     * @param {String} levelName 
     */
    constructor(levelName) {
        this.levelName = levelName
        this.elements = [];
        this.levelDraw = null;
    }


    addElements(thing) {
        this.elements.push(thing);
        this.draw();
    }

    draw() {

        if(!this.levelDraw){
            this.levelDraw = document.createElement('div');
            }
    
            if(this.levelDraw){
                this.levelDraw.innerHTML = '';
            }

            for (let i = 0; i < this.elements.length; i++) {
            this.levelDraw.appendChild(this.elements[i].elementDraw);
        }

    }

}


