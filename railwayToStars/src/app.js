
// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// |||||||||||| avant tout... musique ! |||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||


let isMusic = document.querySelector('#musicBtn');
isMusic.addEventListener('click', function () {
    toggleMusic();
});

// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||| recuperer l'espace de jeu ||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||

let gameBoard = document.querySelector('#gameBoard');

// ||||||||||||||||||||||||||||||||||||||||||||||||||||

// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||| nomer et creer le niveau |||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||

// ||||||||||||||||||||||||||||||||||||||||||||||||||||

let level1Name = '1grandeOurse';
let level1 = new Levels(level1Name);

// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// |||||||||| creer le message de bienvenue |||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||

let welcomeMessage = document.createElement('div');
welcomeMessage.classList.add('welcomeMessage', 'element');
let h2 = document.createElement('h2');
h2.textContent = 'Railway To Stars';

let p1 = document.createElement('p');
p1.textContent = 'Tu gères un réseau de train entre les étoiles. Il faut gérer les aiguillages et les arrêts pour former les bonnes constellations et amener les passagers à leurs destinations dans les temps !';

let p2 = document.createElement('p');
p2.textContent = 'Clique sur une étoile pour la selectionner. Puis sur les boutons de rotation pour la mettre en place';

let p3 = document.createElement('p');
p3.textContent = ' ok ';
p3.setAttribute('id', 'bigOk');

let p4 = document.createElement('p');
p4.textContent = '( Jouer sans la musique ? )';
p4.setAttribute('id', 'playWNoSound');

welcomeMessage.appendChild(h2);
welcomeMessage.appendChild(p1);
welcomeMessage.appendChild(p2);
welcomeMessage.appendChild(p3);
welcomeMessage.appendChild(p4);


// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||| creer la constelation (à afficher si gagné) ||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||








// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||| creer les listes d'éléments du niveau ||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||

let starList1 = new StarLists(level1Name);
let clockList1 = new ClockLists(level1Name);
let btnList1 = new BtnLists;

// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||| creer toutes les étoiles dans leur liste |||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||

starList1.createStar(1, 90, 108);
starList1.createStar(2, 253, 153);
starList1.createStar(3, 316, 246);
starList1.createStar(4, 406, 351);
starList1.createStar(5, 650, 451);
starList1.createStar(6, 561, 556);
starList1.createStar(7, 379, 463);

// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||||||| creer les horloges |||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||

let currentTime1 = new Clocks(14, 30, 365, 700);
let dueTime1 = new Clocks(14, 30, 640, 112);

let calcDueTime = 3;

for (i = 0; i < starList1.starList.length; i++) {
    console.log(starList1.starList[i].currentRotation);

    if (starList1.starList[i].currentRotation < 5) {
        calcDueTime += starList1.starList[i].currentRotation;
    }
    if (starList1.starList[i].currentRotation === 5) {
        calcDueTime += 3;
    }
    if (starList1.starList[i].currentRotation === 6) {
        calcDueTime += 2;
    }
    if (starList1.starList[i].currentRotation === 7) {
        calcDueTime += 1;
    }
    if (starList1.starList[i].currentRotation === 8) {
        calcDueTime += 0;
    }
}
console.log(calcDueTime);


for (i = 0; i < calcDueTime; i++) {
    dueTime1.addTime();
}

// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||| ajouter les horloges dans leur liste |||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||

clockList1.addClock(currentTime1);
clockList1.addClock(dueTime1);


// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// |||||||||||||| creer les boutons |||||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||

let btnCW = new Btns(true, 110, 680);
let btnACW = new Btns(false, 594, 680);

// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||| ajouter les boutons dans leur liste ||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||

btnList1.addBtn(btnCW);
btnList1.addBtn(btnACW);

// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||| ajouter les listes d'éléments au niveau ||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||

level1.addElements(starList1);
level1.addElements(clockList1);
level1.addElements(btnList1);

// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// |||||||||||||| "dessiner" le niveau ||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// |||||   !!! si l'une des listes est vide !!!   |||||
// ||||  la generation du  niveau ne marchera pas  ||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||

gameBoard.appendChild(level1.levelDraw);
gameBoard.appendChild(welcomeMessage);


// ||||||||||||||||||||||||||||||||||||||||||||||||||||

// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// |||||||||||||| Maintenant on joue ! ||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||
// ||||||||||||||||||||||||||||||||||||||||||||||||||||

// ||||||||||||||||||||||||||||||||||||||||||||||||||||

p3.addEventListener('click', function () {
    welcomeMessage.classList.add('hide');
    toggleMusic();
});
p4.addEventListener('click', function () {
    welcomeMessage.classList.add('hide');
});



for (let i = 0; i < starList1.starList.length; i++) {

    starList1.starList[i].starDraw.addEventListener('click', function () {

        for (let j = 0; j < starList1.starList.length; j++) {
            starList1.starList[j].unSelectStar()
        }

        starList1.starList[i].selectStar()
    });
}


let late = 0

btnACW.btnDraw.addEventListener('click', function () {

    for (let i = 0; i < starList1.starList.length; i++) {

        if (starList1.starList[i].isStarSelect) {

            starList1.starList[i].rotate("aClkW");
            currentTime1.addTime(dueTime1);
        }

        if (currentTime1.isLate(dueTime1) === "you're late.") {
            currentTime1.clockDraw.classList.add('ohNoYouRLate');
            late += 5;
        }

        console.log();
        
        starList1.isWin();

        if (starList1.isWin()) {

            OMGYOUWIN();
        }
    }
})



btnCW.btnDraw.addEventListener('click', function () {

    for (let i = 0; i < starList1.starList.length; i++) {

        if (starList1.starList[i].isStarSelect) {

            starList1.starList[i].rotate("clkW");
            currentTime1.addTime(dueTime1);

        }

        if (currentTime1.isLate(dueTime1) === "you're late.") {
            currentTime1.clockDraw.classList.add('ohNoYouRLate');
            late += 5
        }
    }

    starList1.isWin();

    if (starList1.isWin()) {

        OMGYOUWIN();
    }
});






function OMGYOUWIN() {

    let constellationWhoShowIfWin = document.createElement('div');

    let grandeOurse = document.createElement('img');
    grandeOurse.src = './medias/1grandeOurse/grandeOurse.png';
    grandeOurse.classList.add('grandeOurse', 'grandeOurseWin');


    let grandeOurseHalo = document.createElement('img');
    grandeOurseHalo.src = './medias/1grandeOurse/grandeOurseHalo.png';
    grandeOurseHalo.classList.add('grandeOurse', 'grandeOurseWinHalo');
    

    let youWin = document.createElement('div');
    youWin.setAttribute('id', 'youWin');


    let p1YouWin = document.createElement('p');
    p1YouWin.classList.add('omgYouWin');
    p1YouWin.setAttribute('id', 'p1YouWin');


    if (late === 0) {
        p1YouWin.textContent = `Bravo ! Ton train part à l'heure !`;
    }
    if (late > 0) {
        p1YouWin.textContent = `Oups... Ton train part avec un retard de ${late} minutes`;
    }

    let p2YouWin = document.createElement('p');
    p2YouWin.classList.add('omgYouWin');
    p2YouWin.setAttribute('id', 'p2YouWin');
    p2YouWin.innerHTML = `<i class="far fa-star"></i> Un grand merci à UnBot pour l'idée <i class="far fa-star"></i>`;

    let p3YouWin = document.createElement('p');
    p3YouWin.classList.add('omgYouWin');
    p3YouWin.setAttribute('id', 'p3YouWin');
    p3YouWin.textContent = `♫ À Monplaisir pour la musique ♫`;

    let p4YouWin = document.createElement('p');
    p4YouWin.classList.add('omgYouWin');
    p4YouWin.setAttribute('id', 'p4YouWin');
    p4YouWin.textContent = `♥ Et à toi d'avoir joué ♥`;

    youWin.appendChild(p1YouWin);
    youWin.appendChild(p2YouWin);
    youWin.appendChild(p3YouWin);
    youWin.appendChild(p4YouWin);

    constellationWhoShowIfWin.appendChild(grandeOurse);
    constellationWhoShowIfWin.appendChild(grandeOurseHalo);
    constellationWhoShowIfWin.appendChild(youWin);

    gameBoard.appendChild(constellationWhoShowIfWin);

}


function toggleMusic() {
    let symbol = document.querySelector('#musicSymbol');
    let music = document.querySelector('#music');

    if (music.paused) {
        music.play();

        symbol.classList.remove('fa-volume-off');
        symbol.classList.add('fa-volume-up');

    }
    else {
        music.pause();

        symbol.classList.remove('fa-volume-up');
        symbol.classList.add('fa-volume-off');


    }

}